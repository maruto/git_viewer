package com.example.git_viewer.repositories.domain.service

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header

interface GetRepositoriesService {
    @GET("user/repos")
    fun getRepositories(
        @Header("Authorization") authHeader: String?,
    ): Call<String>
}