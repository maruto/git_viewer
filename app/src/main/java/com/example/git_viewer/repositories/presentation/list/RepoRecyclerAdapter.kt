package com.example.git_viewer.repositories.presentation.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.git_viewer.R
import com.example.git_viewer.common.data.model.Repo

class RepoRecyclerAdapter(
    private val repositories: List<Repo>,
    private val onClickListener: OnRepositoriesListClickListener,
) : RecyclerView.Adapter<RepoRecyclerAdapter.RepositoriesListViewHolder>() {

    interface OnRepositoriesListClickListener {
        fun onStateClick(repositoryDescription: Repo, position: Int)
    }


    class RepositoriesListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val itemTitle: TextView = itemView.findViewById(R.id.item_title)
        val itemDescription: TextView = itemView.findViewById(R.id.item_description)
        val itemLanguageTag: TextView = itemView.findViewById(R.id.item_language_tag)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepositoriesListViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.repositories_list_item, parent, false)
        return RepositoriesListViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: RepositoriesListViewHolder, position: Int) {
        holder.itemTitle.text = repositories[position].name
        holder.itemDescription.text = repositories[position].description
        holder.itemLanguageTag.text = repositories[position].language
        holder.itemView.setOnClickListener {
            onClickListener.onStateClick(repositories[position], position)
        }

    }

    override fun getItemCount() = repositories.size
}