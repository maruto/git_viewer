package com.example.git_viewer.repositories.domain.repositories

import com.example.git_viewer.common.data.model.Repo
import com.example.git_viewer.common.domain.repository.IDeleteTokenRepository

interface IRepoRepository : IDeleteTokenRepository {
    fun getRepoList() : ArrayList<Repo>
}