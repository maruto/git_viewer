package com.example.git_viewer.repositories.presentation.list

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.git_viewer.R
import com.example.git_viewer.common.data.model.Repo

class RepoListFragment(private var repoList: ArrayList<Repo>): Fragment(R.layout.fragment_repositories_list) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val recyclerView: RecyclerView = view.findViewById(R.id.repositories_recycle_view)
        recyclerView.layoutManager = LinearLayoutManager(activity)

        val currencyListRecyclerViewAdapter = RepoRecyclerAdapter(
            repoList,
            object : RepoRecyclerAdapter.OnRepositoriesListClickListener {
                override fun onStateClick(repositoryDescription: Repo, position: Int) {
                    val bundle = Bundle()
                    bundle.putInt("position", position) //TODO передача данных между экранами
                    Navigation.findNavController(view)
                        .navigate(R.id.action_repositoriesFragment_to_detailFragment, bundle)
                }
            }
        )
        recyclerView.adapter = currencyListRecyclerViewAdapter
    }
}