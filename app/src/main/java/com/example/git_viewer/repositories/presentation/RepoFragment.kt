package com.example.git_viewer.repositories.presentation

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.example.git_viewer.R
import com.example.git_viewer.auth.presentation.AuthViewModel
import com.example.git_viewer.auth.presentation.AuthViewModelFactory
import com.example.git_viewer.common.presentation.error.ErrorFragment
import com.example.git_viewer.common.presentation.loading.LoadingFragment
import com.example.git_viewer.repositories.presentation.list.RepoListFragment

class RepoFragment : Fragment(R.layout.fragment_repositories) {
    private lateinit var vm: RepoListViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm = ViewModelProvider(
            this,
            RepoViewModelFactory(this.requireContext())
        )[RepoListViewModel::class.java]

        val logOutButton: ImageView = view.findViewById(R.id.repositoriesLogOutIcon)
        logOutButton.setOnClickListener {
            vm.onLogOutTap()
            Navigation.findNavController(view)
                .navigate(R.id.action_repositoriesFragment_to_authFragment)
        }


        vm.uiState.observe(viewLifecycleOwner) { uiState ->
            when (uiState) {
                0 -> parentFragmentManager.beginTransaction().replace(
                        R.id.repositories_frame_layout,
                        LoadingFragment(),
                    ).addToBackStack(null).commit()
                1, 3, 4 -> {
                    createChildErrorFragment()
                }
                2 -> {
                    parentFragmentManager.beginTransaction().replace(
                        R.id.repositories_frame_layout,
                        RepoListFragment(vm.repo),
                    ).addToBackStack(null).commit()
                }
            }
        }
    }

    private fun createChildErrorFragment() {
        val colorId: Int
        val imageId: Int
        when (vm.message) {
            "Empty" -> {
                colorId = R.color.blue
                imageId = R.drawable.empty_repo_list
            }
            "Connection error" -> {
                colorId = R.color.red_error
                imageId = R.drawable.no_internet
            }
            else -> {
                colorId = R.color.red_error
                imageId = R.drawable.erorr
            }
        }
        parentFragmentManager.beginTransaction().replace(
            R.id.repositories_frame_layout,
            ErrorFragment.newInstance(
                tryAgainFunction = { vm.loadRepoList() },
                title = vm.message,
                subTitle = vm.subMessage,
                buttonText = vm.btnText,
                color_id = colorId,
                image_id = imageId
            )
        ).addToBackStack(null).commit()
    }
}







































