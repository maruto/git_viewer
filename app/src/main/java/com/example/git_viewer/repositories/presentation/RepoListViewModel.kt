package com.example.git_viewer.repositories.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.git_viewer.common.data.model.Repo
import com.example.git_viewer.common.exception.NoInternetException
import com.example.git_viewer.repositories.domain.repositories.IRepoRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class RepoListViewModel(
    private val repository: IRepoRepository,
): ViewModel() {

    val uiState: MutableLiveData<Int> = MutableLiveData()

    var message: String
        private set

    var subMessage: String
        private set

    var btnText: String
        private set

    var repo: ArrayList<Repo> = ArrayList()
        private set

    init {
        message = ""
        subMessage = ""
        btnText = ""
        loadRepoList()
    }

    fun onLogOutTap() {
        repository.deleteToken()
    }

    fun loadRepoList() {
        viewModelScope.launch {
            uiState.value = 0 //состояние загрузки
            try {
                val result = withContext(Dispatchers.IO) {
                    repository.getRepoList()
                }
                if(result.isEmpty()) {
                    message = "Empty"
                    subMessage= "No repositories at the moment"
                    btnText = "REFRESH"
                    uiState.value = 1 //состояние c пустым репозиторием
                } else {
                    repo = result
                    uiState.value = 2 //всё ок
                }
            } catch (e: NoInternetException) {
                message = "Connection error"
                subMessage= "Check your Internet connection"
                btnText = "RETRY"
                uiState.value = 3
            } catch (e: Exception) {
                message = "Something error"
                subMessage= "Something goes wrong"
                btnText = "RETRY"
                uiState.value = 4
            }
        }
    }
}