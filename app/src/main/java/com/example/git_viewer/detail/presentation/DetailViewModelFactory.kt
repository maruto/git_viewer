package com.example.git_viewer.detail.presentation

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.git_viewer.common.data.repository.RepoListRepository

class DetailViewModelFactory(
    context: Context,
) : ViewModelProvider.Factory {
    private val repository by lazy(LazyThreadSafetyMode.NONE) {
        RepoListRepository.instance
    }

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return DetailViewModel(
            repository = repository,
        ) as T
    }
}