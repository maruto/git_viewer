package com.example.git_viewer.detail.domain.repositories

import com.example.git_viewer.common.domain.repository.IDeleteTokenRepository

interface IDetailRepository: IDeleteTokenRepository {

    fun getReadMe() : String
}