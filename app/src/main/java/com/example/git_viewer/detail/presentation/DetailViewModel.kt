package com.example.git_viewer.detail.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.git_viewer.common.data.model.Repo
import com.example.git_viewer.detail.domain.repositories.IDetailRepository

class DetailViewModel(
    private val repository: IDetailRepository,
): ViewModel() {

    val uiState: MutableLiveData<Int> = MutableLiveData()

    var link: String
        private set

    var licence: String
        private set

    var detailStars: String
        private set

    var detailForks: String
        private set

    var detailWatchers: String
        private set

    var repo: ArrayList<Repo> = ArrayList()
        private set

    init {
        uiState.value = 0 //состояние загрузки
        link = ""
        licence = ""
        detailStars = ""
        detailForks = ""
        detailWatchers = ""
    }

    fun getRepo(index: Int){
        uiState.value = 0
        val repo = repository.getRepo(index)
        link = repo.url
        licence = repo.license
        detailStars = repo.stargazersCount.toString()
        detailForks = repo.forksCount.toString()
        detailWatchers = repo.watchersCount.toString()
    }

    fun onLogOutTap() {
        repository.deleteToken()
    }
}