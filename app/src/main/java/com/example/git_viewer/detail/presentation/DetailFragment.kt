package com.example.git_viewer.detail.presentation

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.example.git_viewer.R
import com.example.git_viewer.detail.presentation.info.DetailInfoFragment
import com.example.git_viewer.common.presentation.error.ErrorFragment
import com.example.git_viewer.common.presentation.loading.LoadingFragment
import com.example.git_viewer.repositories.presentation.list.RepoListFragment

class DetailFragment: Fragment(R.layout.fragment_detail) {
    private lateinit var vm: DetailViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vm = ViewModelProvider(
            this,
            DetailViewModelFactory(this.requireContext())
        )[DetailViewModel::class.java]

        val logOutButton: ImageView = view.findViewById(R.id.detailLogOutIcon)
        logOutButton.setOnClickListener {
            vm.onLogOutTap()
            Navigation.findNavController(view)
                .navigate(R.id.action_detailFragment_to_authFragment)
        }
        val backButton: ImageView = view.findViewById(R.id.detailBackArrow)
        backButton.setOnClickListener {
            Navigation.findNavController(view)
                .navigate(R.id.action_detailFragment_to_repositoriesFragment)
        }

        val index: Int = arguments?.getInt("position")!!
        vm.getRepo(index)
        //TODO передеать данные на нужный экран


        //вызываеи тут нужный фрагмент ошибка загрузка список
        parentFragmentManager.commit {
            setReorderingAllowed(false)
            add(R.id.detail_info_frame_layout,
                ErrorFragment.newInstance(
                    tryAgainFunction = {
                        parentFragmentManager.commit{
                            //тут должна быть функция которая будет вызывать загрузку а потом либо тот же экран ошики либо пусто либо нужный реадми
                            setReorderingAllowed(false)
                            replace(
                                R.id.detail_info_frame_layout,
                                DetailInfoFragment.newInstance(
                                    link = "https://github.com/icerockdev/moko-resources",
                                    licence = "Apache-2.0",
                                    detailStars = "11",
                                    detailForks = "20",
                                    detailWatchers = "345",
                                ))
                        }
                    },
                    title = "Connection error",
                    subTitle = "Check your Internet connection",
                    buttonText = "retry",
                    color_id = R.color.red_error,
                    image_id = R.drawable.no_internet
                ))
        }
    }
}














