package com.example.git_viewer.detail.presentation.info

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import com.example.git_viewer.R
import com.example.git_viewer.detail.presentation.info.readme.DetailInfoReadMeFragment
import com.example.git_viewer.common.presentation.error.ErrorFragment

class DetailInfoFragment: Fragment(R.layout.fragment_detail_info) {
    //TODO error loading readme buttons
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val linkText: TextView = view.findViewById(R.id.detail_link)
        val licenceText: TextView = view.findViewById(R.id.detail_license)
        val detailStarsText: TextView = view.findViewById(R.id.detail_stars_text)
        val detailForksText: TextView = view.findViewById(R.id.detail_forks_text)
        val detailWatchersText: TextView = view.findViewById(R.id.detail_watchers_text)


        parentFragmentManager.commit {
            setReorderingAllowed(true)

            linkText.text = requireArguments().getString(LINK)
            licenceText.text = requireArguments().getString(LICENCE)
            detailStarsText.text = requireArguments().getString(DETAIL_STARTS)
            detailForksText.text = requireArguments().getString(DETAIL_FORKS)
            detailWatchersText.text = requireArguments().getString(DETAIL_WATCHERS)

            add(R.id.detail_info_readme_frame_layout,
                ErrorFragment.newInstance(
                    tryAgainFunction = {
                        parentFragmentManager.commit{
                            //тут должна быть функция которая будет вызывать загрузку а потом либо тот же экран ошики либо пусто либо нужнйы реадми
                            replace(R.id.detail_info_readme_frame_layout, DetailInfoReadMeFragment())
                        }
                    },
                    title = "Connection error",
                    subTitle = "Check your Internet connection",
                    buttonText = "retry",
                    color_id = R.color.red_error,
                    image_id = R.drawable.no_internet
                ))
        }
    }


    companion object {
        @JvmStatic
        private val LINK: String = "LINK"

        @JvmStatic
        private val LICENCE: String = "LICENCE"

        @JvmStatic
        private val DETAIL_STARTS: String = "DETAIL_STARTS"

        @JvmStatic
        private val DETAIL_FORKS: String = "DETAIL_FORKS"

        @JvmStatic
        private val DETAIL_WATCHERS: String = "DETAIL_WATCHERS"

        @JvmStatic
        fun newInstance(
            link: String,
            licence: String,
            detailStars: String,
            detailForks: String,
            detailWatchers: String,
        ): DetailInfoFragment {
            val args = Bundle().apply {
                putString(LINK, link)
                putString(LICENCE, licence)
                putString(DETAIL_STARTS, detailStars)
                putString(DETAIL_FORKS, detailForks)
                putString(DETAIL_WATCHERS, detailWatchers)
            }
            val fragment = DetailInfoFragment()
            fragment.arguments = args
            return fragment
        }
    }
}