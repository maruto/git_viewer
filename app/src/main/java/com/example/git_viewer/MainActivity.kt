package com.example.git_viewer

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.git_viewer.common.KeyValueStorage
import com.example.git_viewer.common.data.repository.RepoListRepository

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        RepoListRepository.initialize(context = this, storage = KeyValueStorage(this))
    }
}