package com.example.git_viewer.common.data.model

data class Repo(
    val id: Int,
    val name: String,
    val url: String,
    val description: String,
    val watchersCount: Int,
    val forksCount: Int,
    val stargazersCount: Int,
    val language:String,
    val license: String,
    val ownerLogin: String,
)