package com.example.git_viewer.common.data.mapper

import com.example.git_viewer.common.data.model.Repo
import org.json.JSONObject

object Mapper {
    fun jsonToModel(jsonRepoObject: JSONObject): Repo {            val language = jsonRepoObject.getString("language")
        val license = jsonRepoObject.getString("language")
        return Repo(
            id = jsonRepoObject.getInt("id"),
            name = jsonRepoObject.getString("name"),
            url = jsonRepoObject.getString("html_url"),
            description = jsonRepoObject.getString("description"),
            watchersCount = jsonRepoObject.getInt("watchers_count"),
            forksCount = jsonRepoObject.getInt("forks_count"),
            stargazersCount = jsonRepoObject.getInt("stargazers_count"),
            language = when (language) {
                "null" -> ""
                else -> language
            },
            license = when (license) {
                "null" -> ""
                else -> license
            },
            ownerLogin = jsonRepoObject.getJSONObject("owner").getString("login"),
        )
    }
}