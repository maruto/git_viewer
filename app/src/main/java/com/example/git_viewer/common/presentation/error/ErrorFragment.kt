package com.example.git_viewer.common.presentation.error

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.git_viewer.R
import java.io.Serializable

class ErrorFragment: Fragment(R.layout.fragment_error) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val errorImage: ImageView = view.findViewById(R.id.err_image)
        val titleText: TextView = view.findViewById(R.id.err_title)
        val subTitleText: TextView = view.findViewById(R.id.err_subTitle)
        val errorButton: Button = view.findViewById(R.id.err_btn)

        try {
            errorImage.setImageResource(requireArguments().getInt(IMAGE_ID))

            //TODO fix deprecated
            val callback: () -> Unit = arguments?.getSerializable(CALLBACK_FUNCTION) as () -> Unit
            errorButton.setOnClickListener { callback.invoke() }
            errorButton.text = requireArguments().getString(BUTTON_TEXT)

            titleText.text = requireArguments().getString(TEXT_TITLE)

            subTitleText.text = requireArguments().getString(TEXT_SUBTITLE)

        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    companion object {
        @JvmStatic
        private val CALLBACK_FUNCTION: String = "CALLBACK_FUNCTION"

        @JvmStatic
        private val TEXT_TITLE: String = "TEXT_TITLE"

        @JvmStatic
        private val TEXT_SUBTITLE: String = "TEXT_SUBTITLE"

        @JvmStatic
        private val BUTTON_TEXT: String = "BUTTON_TEXT"

        @JvmStatic
        private val IMAGE_COLOR_ID: String = "IMAGE_COLOR_ID"

        @JvmStatic
        private val IMAGE_ID: String = "IMAGE_ID"

        @JvmStatic
        fun newInstance(
            tryAgainFunction: () -> Unit,
            title: String,
            subTitle: String,
            buttonText: String,
            color_id: Int,
            image_id: Int,
        ): ErrorFragment {
            val args = Bundle().apply {
                putSerializable(CALLBACK_FUNCTION, tryAgainFunction as Serializable)
                putString(TEXT_TITLE, title)
                putString(TEXT_SUBTITLE, subTitle)
                putString(BUTTON_TEXT, buttonText)
                putInt(IMAGE_COLOR_ID, color_id)
                putInt(IMAGE_ID, image_id)
            }
            val fragment = ErrorFragment()
            fragment.arguments = args
            return fragment
        }
    }
}