package com.example.git_viewer.common

import android.content.Context
import android.content.SharedPreferences

class KeyValueStorage(private val context: Context) {

    private val FILE_NAME = "Prefs"
    private val KEY = "KEY"

    var key: String? = null
        private set
        get() {
            val sPref = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE)
            return sPref.getString(KEY, null)
        }

    /***
     * Добавление ключа
     */
     fun addKey(newKey: String) {
        if(!key.equals(newKey)) {
            key = newKey
            val sPref = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE)
            val ed: SharedPreferences.Editor = sPref.edit()
            ed.putString(KEY, newKey)
            ed.apply()
        }
    }

    /***
     * Удаление ключа
     */
     fun deleteKey() {
        key = null
        val sPref = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE)
        sPref.edit().clear().apply()
    }
}