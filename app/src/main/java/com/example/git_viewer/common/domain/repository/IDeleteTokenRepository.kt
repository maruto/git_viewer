package com.example.git_viewer.common.domain.repository

import com.example.git_viewer.common.data.model.Repo

interface IDeleteTokenRepository {
    fun deleteToken()

    fun getRepo(index: Int): Repo
}