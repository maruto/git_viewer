package com.example.git_viewer.common

import com.example.git_viewer.auth.domain.service.CheckTokenService
import com.example.git_viewer.repositories.domain.service.GetRepositoriesService
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory


object RetrofitClient {

    private val retrofit by lazy {
        Retrofit.Builder()
            .baseUrl("https://api.github.com/")
            .addConverterFactory(ScalarsConverterFactory.create())
            .build()
    }

    val getRepositoriesService: GetRepositoriesService by lazy {
        retrofit.create(GetRepositoriesService::class.java)
    }

    val checkTokenService: CheckTokenService by lazy {
        retrofit.create(CheckTokenService::class.java)
    }
}
