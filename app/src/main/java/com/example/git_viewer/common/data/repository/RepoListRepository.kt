package com.example.git_viewer.common.data.repository

import android.content.Context
import com.example.git_viewer.common.data.mapper.Mapper.jsonToModel
import com.example.git_viewer.auth.domain.repository.IAuthRepository
import com.example.git_viewer.common.KeyValueStorage
import com.example.git_viewer.common.NetworkChecker
import com.example.git_viewer.common.RetrofitClient
import com.example.git_viewer.common.exception.BadTokenException
import com.example.git_viewer.common.data.model.Repo
import com.example.git_viewer.common.domain.repository.IDeleteTokenRepository
import com.example.git_viewer.common.exception.NoInternetException
import com.example.git_viewer.detail.domain.repositories.IDetailRepository
import com.example.git_viewer.repositories.domain.repositories.IRepoRepository
import org.json.JSONArray
import java.util.concurrent.atomic.AtomicBoolean

class RepoListRepository private constructor(
    context: Context,
    private val storage: KeyValueStorage,
)   :   IAuthRepository,
        IRepoRepository,
        IDetailRepository,
        IDeleteTokenRepository {
    private val networkChecker: NetworkChecker

    private var repoList: ArrayList<Repo> = ArrayList()

    private var token: String = ""

    init {
        networkChecker = NetworkChecker(context = context)
    }

    override fun checkToken(token: String): Boolean {
        if (!networkChecker.isNetworkAvailable()) {
            throw NoInternetException()
        }
        this.token = token
        val response = RetrofitClient.checkTokenService.checkToken("Bearer $token").execute()
        return if(response.isSuccessful){
            storage.addKey(token)
            true
        } else {
            false
        }
    }

    override fun getRepoList(): ArrayList<Repo> {
        if (!networkChecker.isNetworkAvailable()) {
            throw NoInternetException()
        }

        val response =
            RetrofitClient.getRepositoriesService.getRepositories("Bearer $token").execute()

        if (response.isSuccessful) {
            val stringResponse = response.body()
            if (stringResponse == null) {
                throw Exception()
            } else {
                val jsonRepositoriesList = JSONArray(stringResponse)
                val tempList: ArrayList<Repo> = ArrayList()
                if (jsonRepositoriesList.length() != 0) {
                    for (i in 0 until jsonRepositoriesList.length()) {
                        tempList.add(jsonToModel(jsonRepositoriesList.getJSONObject(i)))
                    }
                }
                repoList = tempList
            }
        } else if (response.code() == 401) {
            throw BadTokenException()
        } else {
            throw Exception()
        }
        return repoList
    }

    override fun deleteToken() {
        storage.deleteKey()
    }

    override fun getRepo(index: Int): Repo {
        return repoList[index]
    }

    //TODO
    override fun getReadMe(): String {
        TODO("Not yet implemented")
    }

    companion object {
        @Volatile
        private lateinit var INSTANCE: RepoListRepository
        private val initialized = AtomicBoolean(false)

        val instance: RepoListRepository get() = INSTANCE

        fun initialize(context: Context, storage: KeyValueStorage) {
            if (!initialized.getAndSet(true)) {
                INSTANCE = RepoListRepository(context, storage)
            }
        }
    }
}