package com.example.git_viewer.auth.domain.service

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header

interface CheckTokenService {
    @GET(".")
    fun checkToken(
        @Header("Authorization") authHeader: String?,
    ): Call<String>
}