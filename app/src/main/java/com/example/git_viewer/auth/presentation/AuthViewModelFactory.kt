package com.example.git_viewer.auth.presentation

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.git_viewer.common.data.repository.RepoListRepository

class AuthViewModelFactory (
    context: Context,
) : ViewModelProvider.Factory {
    private val repository by lazy(LazyThreadSafetyMode.NONE) {
        RepoListRepository.instance
    }

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return AuthViewModel(
            repository = repository,
        ) as T
    }
}