package com.example.git_viewer.auth.domain.repository

interface IAuthRepository {

    fun checkToken(token: String): Boolean
}