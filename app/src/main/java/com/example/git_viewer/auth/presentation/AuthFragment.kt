package com.example.git_viewer.auth.presentation

import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.example.git_viewer.R
import com.google.android.material.textfield.TextInputLayout

class AuthFragment : Fragment(R.layout.fragment_auth) {
    private lateinit var vm: AuthViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val button: CardView = view.findViewById(R.id.auth_sign_btn)
        val btnText: TextView = view.findViewById(R.id.btnText)
        val progressBar: ProgressBar = view.findViewById(R.id.progressBar)
        val editText: EditText = view.findViewById(R.id.auth_text_input_edit_text)
        val textInput: TextInputLayout = view.findViewById(R.id.auth_text_input_layout)

        vm = ViewModelProvider(
            this,
            AuthViewModelFactory(this.requireContext())
        )[AuthViewModel::class.java]


        button.setOnClickListener {
            if (editText.text.isNotEmpty()) {
                vm.onSignButtonPressed(editText.text.toString())
                vm.uiState.observe(viewLifecycleOwner) { uiState ->
                    when (uiState) {
                        0 -> {
                            btnText.visibility = View.VISIBLE
                            progressBar.visibility = View.INVISIBLE
                        }
                        1 -> {
                            btnText.visibility = View.INVISIBLE
                            progressBar.visibility = View.VISIBLE
                        }
                        2 -> {
                            Navigation.findNavController(view)
                                .navigate(R.id.action_authFragment_to_repositoriesFragment)
                        }
                        3 -> {
                            btnText.visibility = View.VISIBLE
                            progressBar.visibility = View.INVISIBLE
                            textInput.error = vm.errorMessage
                        }
                        4 -> {
                            btnText.visibility = View.VISIBLE
                            progressBar.visibility = View.INVISIBLE
                            showToast(vm.errorMessage)
                        }
                        5 -> {
                            btnText.visibility = View.VISIBLE
                            progressBar.visibility = View.INVISIBLE
                            textInput.error = vm.errorMessage
                        }
                    }
                }
            }
        }
    }

    private fun showToast(message: String) {
        Toast.makeText(
            context,
            message,
            Toast.LENGTH_SHORT
        ).show()
    }
}