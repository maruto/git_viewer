package com.example.git_viewer.auth.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.git_viewer.R
import com.example.git_viewer.auth.domain.repository.IAuthRepository
import com.example.git_viewer.common.exception.NoInternetException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class AuthViewModel(
    private val repository: IAuthRepository,
) : ViewModel() {
    val uiState: MutableLiveData<Int> = MutableLiveData()

    var errorMessage: String
        private set

    init {
        uiState.value = 0
        errorMessage = ""
    }

    fun onSignButtonPressed(token: String) {
        viewModelScope.launch {
            uiState.value = 1 //состояние загрузки
            try {
                val result = withContext(Dispatchers.IO) {
                    repository.checkToken(token)
                }
                if (result) {
                    uiState.value = 2 //состояние перехода на новую страницу
                } else {
                    errorMessage = "Invalid token"
                    uiState.value = 3 //состояние с невалидным токеном
                }
            } catch (e: NoInternetException) {
                errorMessage = "Connection error"
                uiState.value = 4
            } catch (e: Exception) {
                errorMessage = "Invalid token or another error"
                uiState.value = 5
            }
        }
    }
}